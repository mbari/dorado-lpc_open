################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/old/lpc1769_41cell/board.c \
../src/old/lpc1769_41cell/board_sysinit.c 

OBJS += \
./src/old/lpc1769_41cell/board.o \
./src/old/lpc1769_41cell/board_sysinit.o 

C_DEPS += \
./src/old/lpc1769_41cell/board.d \
./src/old/lpc1769_41cell/board_sysinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/old/lpc1769_41cell/%.o: ../src/old/lpc1769_41cell/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M3 -I"/work/mcuxpresso/lpc_open/lpc_chip_175x_6x/inc" -I"/work/mcuxpresso/lpc_open/lpc_board_41cell/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


